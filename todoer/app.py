"""
todoer.app
~~~~~~~~~~~~~~~~~~~~~~

Main flask application, uwsgi module to start
"""
from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://localhost/todoer'

#TODO: check how to create a model user sqlalchemy
db = SQLAlchemy(app)


@app.route('/api')
def home():
    # getting query param from request object
    name = request.args.get('name', 'world!')
    return jsonify({'message': 'hello {}'.format(name)})
